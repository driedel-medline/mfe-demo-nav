import { MFELinkNavModule } from './app/mfe-link-nav.module';
import { environment } from './environments/environment';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(MFELinkNavModule)
  .catch(err => console.error(err));