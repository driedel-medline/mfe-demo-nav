import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'mfe-link-nav',
  templateUrl: 'mfe-link-nav.component.html',
  styleUrls: ['mfe-link-nav-component.css']
})

export class MFELinkNavComponent {


  @Output() addNotifEvent = new EventEmitter();
  @Output() deleteNotifEvent = new EventEmitter();

  notification;
  inputObject;
  payLoad = {
    id: 12345
  }
  exampleObject = [
    {'label': 'Home', 'href': 'home', 'target':'p', 'clientroute': true},
    {'label': 'About', 'href': 'about', 'target':'p', 'clientroute': true},
    {'label': 'Services', 'href': 'services', 'target':'p', 'clientroute': true},
    {'label': 'Contact', 'href': 'contact', 'target':'p', 'clientroute': true}
  ]
  // routes = [
  //   'About',
  //   'Services',
  //   'Clients',
  //   'Contact'
  // ]
  addNotificationFromNav() {

    const notifExample = {
      name: 'Item name',
      description: 'This is a test',
    }
    this.addNotifEvent.emit(notifExample);

  
  }

  // changeStore() {
  //   this.store.dispatchEvent(fromNotification.addItem({
  //     name: 'Item name',
  //     description: 'This is a test',
  //   }))
  // }

  deleteNotificationFromNav() {
     
    console.log(this.payLoad, 'this.payload')
   
    this.deleteNotifEvent.emit(this.payLoad);
  }

  
  ngOnInit(): void {
  }

  newFunction(event: MouseEvent, route) {
    //let event = new Event('build');
    event.preventDefault()
    let events = new CustomEvent('build', { bubbles: true, detail: route });
    // Dispatch the event.
    //event.target.dispatchEvent(events);
    event.currentTarget.dispatchEvent(events);
  }
}
