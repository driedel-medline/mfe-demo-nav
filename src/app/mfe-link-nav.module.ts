import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MFELinkNavComponent } from './mfe-link-nav.component';
import { createCustomElement } from '@angular/elements';


// To run local app in browser in isolation
@NgModule({
  imports: [
    BrowserModule,
    FormsModule    
  ],
  declarations: [
    MFELinkNavComponent
  ],
  providers: [],
  exports: [
    MFELinkNavComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [],
  entryComponents: [MFELinkNavComponent]
})

export class MFELinkNavModule {
  constructor(injector: Injector) {
    const customElement = createCustomElement(MFELinkNavComponent, {injector: injector});
    customElements.define('mfe-link-nav', customElement);
  }

  ngDoBootstrap(){}
}
