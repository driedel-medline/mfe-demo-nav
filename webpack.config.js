const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  output: {
    uniqueName: "mfeLinkNav"
  },
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "mfeLinkNav",

      filename: "remoteEntry.js",
      exposes: {
        './MFELinkNavModule': './src/app/mfe-link-nav.module.ts',
        './MFELinkNavComponent': './src/app/mfe-link-nav.component.ts'
      },
      shared: {
        "@angular/core": { singleton: true, strictVersion: true },
        "@angular/common": { singleton: true, strictVersion: true },
        "@angular/elements": { singleton: true, strictVersion: true },
        "@angular/router" : { singleton: true, strictVersion: true }
      }
    }),
  ],
};
